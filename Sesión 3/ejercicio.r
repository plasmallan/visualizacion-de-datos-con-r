#paquete con la paleta de colores
library(viridis)

#grafico de barras
grafico + geom_bar(aes(reorder(season,total),total, fill = total), stat = 'identity') + 
  scale_fill_viridis_c(option = 'D', direction = 1) + theme_minimal() + coord_flip() +
  labs(x= NULL, y= NULL, fill = 'Total') +
  ggtitle("Cantidad de árboles dibujados por temporada") +
  theme(axis.text.x = element_blank(),
        axis.text.y = element_text(color = 'black', size = 9),
        axis.ticks = element_blank(),
        plot.title = element_text(hjust = 0.1))
